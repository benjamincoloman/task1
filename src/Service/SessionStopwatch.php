<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 7/29/2018
 * Time: 8:17 PM
 */

namespace App\Service;


use Symfony\Component\HttpFoundation\Session\Session;

class SessionStopwatch
{

    public function startStopwatch(Session $session)
    {
        if ($this->isStarted($session))
            return;

        $session->set('startTime', time());
    }

    public function stopStopwatch(Session $session)
    {
        if (!$this->isStarted($session))
            throw new \Exception('Stopwatch is not started');

        $session->set('end', time());
        $session->set('start', $session->get('startTime'));
        $session->remove('startTime');
    }

    public function returnTime(Session $session)
    {
        if (is_null($session->get('end')))
            throw new \Exception('Stopwatch is not stopped');

        $startTime = $session->get('start');
        $endTime = $session->get('end');

        return $endTime-$startTime;
    }

    public function stopAndShow(Session $session)
    {
        $this->stopStopwatch($session);
        return $this->returnTime($session);
    }

    public function isStarted(Session $session)
    {
        if (is_null($session->get('startTime')))
            return false;

        return true;
    }
}