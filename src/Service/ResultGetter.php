<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 7/29/2018
 * Time: 10:24 PM
 */

namespace App\Service;


use Doctrine\ORM\EntityManager;
use App\Entity\Result;

class ResultGetter
{

    public function getTop(EntityManager $em)
    {
        $query = $em->getRepository(Result::class)->createQueryBuilder('x');
        $query->orderBy('x.time', 'ASC');
        $query->setMaxResults(10);
        $query = $query->getQuery();
        return $query->getResult();
    }

}