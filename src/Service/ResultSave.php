<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 7/29/2018
 * Time: 9:43 PM
 */

namespace App\Service;


use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Result;

class ResultSave
{

    public function save(SessionStopwatch $stopwatch, Session $session, User $usr, EntityManager $em)
    {
        $result = new Result();

        $result->setUsername($usr->getUsername());
        $result->setTime($stopwatch->returnTime($session));
        $result->setDate(new \DateTime());

        $em->persist($result);
        $em->flush();
        return;
    }

}