<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 7/29/2018
 * Time: 4:41 PM
 */

namespace App\Controller;

use App\Entity\Result;
use App\Service\ResultGetter;
use App\Service\SessionStopwatch;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\ResultSave;

class DefaultController extends Controller
{

    private $sessionStopwatch;
    private $resultSaver;

    public function __construct(SessionStopwatch $sessionStopwatch, ResultSave $resultSave, ResultGetter $resultGetter)
    {
        $this->sessionStopwatch = $sessionStopwatch;
        $this->resultSaver = $resultSave;
        $this->resultGetter = $resultGetter;
    }

    public function index(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER'))
            return $this->render("init.html.twig");

        $this->sessionStopwatch->startStopwatch($this->get('session'));

        $form = $this->createFormBuilder()
            ->setAction('/submit')
            ->setMethod('POST')
            ->add('Ime', NumberType::class)
            ->add('Prezime', NumberType::class)
            ->add('Godine', NumberType::class)
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->sessionStopwatch->stopStopwatch($this->get('session'));
            $usr = $this->getUser();
            $this->resultSaver->save($this->sessionStopwatch, $this->get('session'), $usr, $this->getDoctrine()->getManager());

            return $this->render("gameChoise.html.twig");
        }

        return $this->render("form.html.twig", ['form' => $form->createView()]);

    }

    public function results()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER'))
            return $this->render("error.html.twig", ['error' => "Must be logged in to see results"]);

        return $this->render('results.html.twig', ['results' => $this->resultGetter->getTop($this->getDoctrine()->getManager())]);
    }


}